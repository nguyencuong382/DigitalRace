#include "traffic.h"

Traffic::Traffic()
{
    this->model = "/home/nc/Desktop/prototype/sign/train.txt";
    this->load();
}

int Traffic::pre(Mat img) {
    vector<int> ids = this->detectMult(img);
    for (int i = 0; i < ids.size(); i++) {
        if (ids[i] >= 0 && ids[i] < 3) return ids[i];
    }
    return -1;
}

void Traffic::load() {
        cout << "Loading model ..." << endl;
        try {
            svm = SVM::create();
            svm = SVM::load(this->model);
            cout << "Model loads successful." << endl;
        } catch (const std::exception& e) {
            cout << "Load model failed." << endl;
        }
}

void Traffic::preprocess(Mat &img, Mat &img2, Mat &hsv, Mat &gray) {
    // cv::resize(img, img, Size(img.cols * this->resizeRatio, img.rows * this->resizeRatio), 0, 0, cv::INTER_LANCZOS4);
//    cv::flip(img, img, 1);
    img.copyTo(img2);
//    cvtColor(img, img2, cv::COLOR_RGB2BGR);
    cvtColor(img2, gray, COLOR_BGR2GRAY);
    cvtColor(img2, hsv, COLOR_BGR2HSV);
}

void filter(Mat &hsv, Mat &mask, Traffic *d){
    inRange(hsv, Scalar(60, 105, 83), Scalar(255,255,255), mask);
     imshow("mask", mask);
}

vector<Rect> Traffic::poolingMult(Mat &mask, Mat &gray, vector<Mat> &outs){
    vector<Rect> rects;
    vector<vector<Point>> contours;
    vector<Rect> boundRect(contours.size());
    vector<Vec4i> hierarchy;

    findContours(mask, contours, hierarchy, RETR_TREE , CHAIN_APPROX_SIMPLE, Point(0, 0) );

    if (contours.size() <= 0) return rects;

    for(unsigned int i = 0; i< contours.size(); i++) {
        Rect rect = boundingRect(contours[i]);
        int minw = 30;
        int minh = 30;
        double rti = rect.width / rect.height;

        if(rect.area() < mask.size().height * mask.size().width
            && rect.area() > minw * minh
//            && rti >= 0.8 && rti <= 1.2
        ) {
            rects.push_back(rect);

            Mat out;
            gray(rect).copyTo(out);
            outs.push_back(out);
        }

    }
    return rects;
}

int Traffic::predict(Mat &test) {

      resize(test,test,Size(64,48));
      HOGDescriptor hog(Size(64,48),Size(8,8), Size(4,4), Size(4,4), 9);
      vector<float> size_;
      hog.compute(test,size_);
      int col= size_.size();
      Mat testMat(1, col, CV_32FC1);
      vector<float> des;
      hog.compute(test,des);
      for (int i = 0; i < col; i++)
      testMat.at<float>(0,i)= des[i];

      return this->svm->predict(testMat);
}


vector<int> Traffic::detectMult(Mat &img) {
    Mat mask, out, img2, hsv, gray;

    this->preprocess(img, img2, hsv, gray);
    filter(hsv, mask, this);

    vector<Mat> outs;
    vector<Rect> objs = this->poolingMult(mask, gray, outs);
    vector<int> ids;

    if(!objs.size() == 0) {
        for (int i = 0; i < outs.size(); i++) {
            Mat out = outs[i];

            cv::rectangle(hsv, objs[i],cv::Scalar(255,255,255));
            int id = predict(out);
            if(id == 0 || id == 1){
                cv::rectangle(hsv, objs[i],cv::Scalar(0,255,255), 2);
            }
            ids.push_back(id);
        }
    }

    cv::imshow("adsf", hsv);
    return ids;
}
