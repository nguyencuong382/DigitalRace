#ifndef SVMDETECTOR_H
#define SVMDETECTOR_H

#endif // SVMDETECTOR_H
#ifndef Traffic_H
#define Traffic_H

#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/ml/ml.hpp>
#include <opencv2/objdetect.hpp>
#include <dirent.h>
#include <string>

using namespace cv;
using namespace ml;
using namespace std;


class Traffic
{
private:

    Ptr<SVM> svm;
    string model;  // model file

    void load();

public:

    Traffic();

    void preprocess(Mat &img, Mat &img2, Mat &hsv, Mat &gray);

    int pre(Mat img);

    vector<Rect> poolingMult(Mat &mask, Mat &gray, vector<Mat> &outs);

    int predict(Mat &test);

    vector<int> detectMult(Mat &img);
};


#endif // Traffic_H
