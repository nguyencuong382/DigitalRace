#include "getparam.h"
#include "config.h"
#include "objconf.h"
#include "traconf.h"
#include "readparam.h"
#include "carconf.h"
#include "greconf.h"
#include "seconf.h"

void init(std::string base_dir){

    std::string file_config_lane = base_dir + "/laneconf.txt";
    std::string file_green_config = base_dir + "/greconf.txt";
    std::string file_config_traffic = base_dir + "/traconf.txt";
    std::string file_config_object = base_dir + "/objconf.txt";
    std::string file_config_car = base_dir + "/carconf.txt";
    std::string file_config_secret_lane = base_dir + "/seconf.txt";

    Config getConfigs;
    std::cout << "lane conf -----------------------------" << std::endl;
    getConfigs.setpath(file_config_lane);
    initLane(getConfigs);

    std::cout <<  std::endl << "se conf -----------------------------" << std::endl;
    getConfigs.setpath(file_config_secret_lane);
    initSe(getConfigs);

    std::cout <<  std::endl << "green conf -----------------------------" << std::endl;
    getConfigs.setpath(file_green_config);
    initGreen(getConfigs);

    std::cout << std::endl << "trafic conf -----------------------------" << std::endl;
    getConfigs.setpath(file_config_traffic);
    initTraffic(getConfigs);

    std::cout << std::endl << "object conf -----------------------------" << std::endl;
    getConfigs.setpath(file_config_object);
    initObject(getConfigs);


    std::cout << std::endl << "car conf -----------------------------" << std::endl;
    getConfigs.setpath(file_config_car);
    initCar(getConfigs);

}

