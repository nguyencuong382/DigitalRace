#include "traconf.h"

namespace tra{
    int WIDTH = 640;
    int HEIGHT = 480;

    int DEBUG = 1;
    int W_RESIZE = 320;
    int H_RESIZE = 240;
    int RESIZE_RATIO = 2;
    double INTERS_AREA_RATIO = 0.1;

    int X_CUT = 0;
    int Y_CUT = 0;
    int Y_BOT_OFFSET = 0;
    int W_CUT = WIDTH;
    int H_CUT = HEIGHT - Y_CUT - Y_BOT_OFFSET;

    double H_RATIO = 2;
    double L_RATIO = 0.5;

    int MIN_CNT = 100;
    int MAX_CNT = 3000;

    int H_LOW = 60;
    int S_LOW = 105;
    int V_LOW = 83;

    int H_HIGH = 255;
    int S_HIGH = 255;
    int V_HIGH = 255;

    cv::Scalar lowHSV(H_LOW, S_LOW, V_LOW);
    cv::Scalar highHSV(H_HIGH, S_HIGH, V_HIGH);

    cv::Rect roi(X_CUT, Y_CUT, W_CUT, H_CUT);
}

void initTraffic(Config &getConfigs){
    tra::DEBUG = getConfigs.find("DEBUG");
    tra::W_RESIZE = getConfigs.find("W_RESIZE");
    tra::H_RESIZE = getConfigs.find("H_RESIZE");
    tra::RESIZE_RATIO = getConfigs.find("RESIZE_RATIO");
    tra::INTERS_AREA_RATIO =(double) (getConfigs.find("INTERS_AREA_RATIO")/100);

    tra::X_CUT = getConfigs.find("X_CUT");
    tra::Y_CUT = getConfigs.find("Y_CUT");
    tra::Y_BOT_OFFSET = getConfigs.find("Y_BOT_OFFSET");
    tra::W_CUT = tra::WIDTH;
    tra::H_CUT = tra::HEIGHT - tra::Y_CUT - tra::Y_BOT_OFFSET;


    tra::MIN_CNT = getConfigs.find("MIN_CNT");
    tra::MAX_CNT = getConfigs.find("MAX_CNT");

    tra::H_RATIO = (double) (getConfigs.find("H_RATIO") / 10);
    tra::L_RATIO = 1/tra::H_RATIO;

    tra::H_LOW = getConfigs.find("H_LOW");
    tra::S_LOW = getConfigs.find("S_LOW");
    tra::V_LOW = getConfigs.find("V_LOW");

    tra::H_HIGH = getConfigs.find("H_HIGH");
    tra::S_HIGH = getConfigs.find("S_HIGH");
    tra::V_HIGH = getConfigs.find("V_HIGH");

    tra::lowHSV  = cv::Scalar(tra::H_LOW, tra::S_LOW, tra::V_LOW);
    tra::highHSV = cv::Scalar(tra::H_HIGH, tra::S_HIGH, tra::V_HIGH);

    tra::roi.x = tra::X_CUT;
    tra::roi.y = tra::Y_CUT;
    tra::roi.width = tra::W_CUT;
    tra::roi.height = tra::H_CUT;

}
