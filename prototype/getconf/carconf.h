#ifndef CARCONF_H
#define CARCONF_H

#include "readparam.h"

namespace car{
extern int SPEED;
extern int ALP;
extern int BET;
extern int GAM;
extern int DEL;
extern int MAX_SPD;
extern int GAM_SPD;
extern int DEL_SPD;
extern int MIN_SPD;
extern int INJECT_SPD;
extern int OBJ_SPD;

extern int GAM_BET;
extern int DEL_GAM;
extern int MAX_GAM_SPD;
extern int GAM_SPD_DEL_SPD;
extern int LOST_LANE_SPD;

extern double START_SPD;
extern double STEP_START_SPD;
extern int STOP_SPD;
extern int STEP_STOP_SPD;

}


namespace refer{
    extern double A;
    extern double O;
    extern double B;
    extern double a;
    extern double b;
}


void initCar(Config &getConfigs);

#endif // CARCONF_H
