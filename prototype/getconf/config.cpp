#include "config.h"
#include <iostream>

namespace conf {
    int WIDTH = 640;
    int HEIGHT = 480;

    // Region of lane
    int X_CUT = 0;
    int Y_CUT = 180;
    int Y_BOT_OFFSET = 0;
    int W_CUT = WIDTH;
    int H_CUT = HEIGHT - Y_CUT - Y_BOT_OFFSET;

    // Region of birdview which will be taked from region of lane
    int W_ROI = 240;
    int X_ROI = (W_CUT - W_ROI)/2;
    int Y_ROI = 0;
    int H_ROI = H_CUT - Y_ROI;


    // config for image processing
    int THRES = 200;
    int NUMLAYERS = 25;
    int MIN_AREA_CNT = 20;
    int MIN_WIDTH_CNT = 20;
    int MIN_AREA_WHITE;

    int DOWN_Y = 50;
    int DOWN_Y_INJEC = 100;
    int MIN_DIS_CAR_OBJ = 10;
    int MAX_DIS_CAR_OBJ = 200;
    int DIS_TURN_OBJ = 20;
    int CLOSE_PROXIMITY_CAR_OBJ = 50;
    int DOWN_Y_LOST_LANE = 200;
    int MAX_ANGLE_COMBINE_LANES = 20;
    int MAX_X_DIS_COMBINE_LINES = 20;
    int MAX_ANGLE_TWO_LANES = 30;

    // Region of layer
    int H_LAYER = H_ROI / NUMLAYERS;
    int MAX_DIS_TWO_POINTS = H_LAYER*3;

    int MIN_POINTS = 5;
    int MIN_POINTS_AFTER_COMBINE = 7;
    int MAX_ANGLE_TWO_POINTS;

    // Distace between two line, use for processing one lane;
    int DIS_GEN_LINE_TOP = 30;
    int DIS_GEN_LINE_BOT = 35;

    // distance between Intersection and lowest line, use for prosseing road injection
    int DIS_ROAD_INJUNCTION = 150;
    int SLOPE_MAX_LINE_INJUNCTION = 9;
    int MIN_X_INJUNCTION = 100;
    int DIS_TOP_INJ = 250;
    int DIS_BOT_INJ = 120;


    int DEBUG = 1;
    int WAIT_KEY = 45 ;
    int WRITE_VIDEO = 1;


    int MAX_DIS_BOT = 100;
    int DOWN_Y_OBJ = 160;
    int DOWN_Y_FAR = 170;

    int FOLLOW_LANE = 0;
    int DIS_FAR_CAR =100;
    int DIS_NEXT_CAR = 100;
    int MIN_LEN_LANE = 50;
    int DIS_EXTERNAL = 50;
    double RATIO_POINTS_LR;
}

namespace imp {
    int ALPHA;
    int F;
    int DIST;
    int Y;
    int SCALE_X;
    int X;
    cv::Rect roi(conf::X_CUT, conf::Y_CUT, conf::W_CUT, conf::H_CUT);
    cv::Rect roiCut(conf::X_ROI, conf::Y_ROI, conf::W_ROI, conf::H_ROI);
    cv::Mat matrixWrap;
    cv::Mat inverMatrixWrap;
}


void initLane(Config &getConfigs){
    conf::WIDTH = getConfigs.find("WIDTH");
    conf::HEIGHT = getConfigs.find("HEIGHT");

    // Region of lane
    conf::X_CUT = getConfigs.find("X_CUT");
    conf::Y_CUT = getConfigs.find("Y_CUT");
    conf::Y_BOT_OFFSET = getConfigs.find("Y_BOT_OFFSET");
    conf::W_CUT = conf::WIDTH;
    conf::H_CUT = conf::HEIGHT - conf::Y_CUT - conf::Y_BOT_OFFSET;

    // Region of birdview which will be taked from region of lane
    conf::W_ROI = getConfigs.find("W_ROI");
    conf::X_ROI = (int)(conf::W_CUT - conf::W_ROI)/2;
    conf::Y_ROI = getConfigs.find("Y_ROI");
    conf::H_ROI = getConfigs.find("H_ROI");


    // config for image processing
    conf::THRES = getConfigs.find("THRES");
    conf::NUMLAYERS = getConfigs.find("NUMLAYERS");
    conf::MIN_AREA_CNT = getConfigs.find("MIN_AREA_CNT");
    conf::MIN_WIDTH_CNT = getConfigs.find("MIN_WIDTH_CNT");
    conf::MIN_AREA_WHITE = getConfigs.find("MIN_AREA_WHITE");

    conf::DOWN_Y = getConfigs.find("DOWN_Y");
    conf::DOWN_Y_INJEC = getConfigs.find("DOWN_Y_INJEC");

    conf::MIN_DIS_CAR_OBJ = getConfigs.find("MIN_DIS_CAR_OBJ");
    conf::MAX_DIS_CAR_OBJ = getConfigs.find("MAX_DIS_CAR_OBJ");
    conf::DIS_TURN_OBJ = getConfigs.find("DIS_TURN_OBJ");
    conf::MAX_DIS_BOT = getConfigs.find("MAX_DIS_BOT");
    conf::CLOSE_PROXIMITY_CAR_OBJ = getConfigs.find("CLOSE_PROXIMITY_CAR_OBJ");

    conf::DOWN_Y_OBJ = getConfigs.find("DOWN_Y_OBJ");
    conf::DOWN_Y_FAR = getConfigs.find("DOWN_Y_FAR");
    conf::DOWN_Y_LOST_LANE = getConfigs.find("DOWN_Y_LOST_LANE");
    conf::MAX_ANGLE_COMBINE_LANES = getConfigs.find("MAX_ANGLE_COMBINE_LANES");
    conf::MAX_X_DIS_COMBINE_LINES = getConfigs.find("MAX_X_DIS_COMBINE_LINES");
    conf::MAX_ANGLE_TWO_LANES = getConfigs.find("MAX_ANGLE_TWO_LANES");
    conf::MAX_ANGLE_TWO_POINTS = getConfigs.find("MAX_ANGLE_TWO_POINTS");
    // Region of layer
    conf::H_LAYER = conf::H_ROI / conf::NUMLAYERS;
    conf::MAX_DIS_TWO_POINTS = getConfigs.find("MAX_DIS_TWO_POINTS");

    conf::RATIO_POINTS_LR = (double)getConfigs.find("RATIO_POINTS_LR") / 10;

    conf::MIN_POINTS = getConfigs.find("MIN_POINTS");
    conf::MIN_POINTS_AFTER_COMBINE = getConfigs.find("MIN_POINTS_AFTER_COMBINE");
    conf::DIS_GEN_LINE_TOP = getConfigs.find("DIS_GEN_LINE_TOP");
    conf::DIS_GEN_LINE_BOT = getConfigs.find("DIS_GEN_LINE_BOT");


    conf::DIS_ROAD_INJUNCTION = getConfigs.find("DIS_ROAD_INJUNCTION");
    conf::SLOPE_MAX_LINE_INJUNCTION = getConfigs.find("SLOPE_MAX_LINE_INJUNCTION");
    conf::MIN_X_INJUNCTION = getConfigs.find("MIN_X_INJUNCTION");
    conf::DIS_TOP_INJ = getConfigs.find("DIS_TOP_INJ");
    conf::DIS_BOT_INJ = getConfigs.find("DIS_BOT_INJ");


    conf::DEBUG = getConfigs.find("DEBUG");

    conf::WAIT_KEY = getConfigs.find("WAIT_KEY");

    conf::WRITE_VIDEO = getConfigs.find("WRITE_VIDEO");
    conf::FOLLOW_LANE = getConfigs.find("FOLLOW_LANE");
    conf::DIS_FAR_CAR = getConfigs.find("DIS_FAR_CAR");
    conf::DIS_NEXT_CAR = getConfigs.find("DIS_NEXT_CAR");
    conf::MIN_LEN_LANE = getConfigs.find("MIN_LEN_LANE");
    conf::DIS_EXTERNAL = getConfigs.find("DIS_EXTERNAL");


    // set imp
    imp::ALPHA = getConfigs.find("ALPHA");
    imp::F = getConfigs.find("F");
    imp::DIST = getConfigs.find("DIST");
    imp::Y = getConfigs.find("Y");
    imp::X = getConfigs.find("X");
    imp::SCALE_X = getConfigs.find("SCALE_X");
    imp::roi.x = conf::X_CUT;
    imp::roi.y = conf::Y_CUT;
    imp::roi.width = conf::W_CUT;
    imp::roi.height = conf::H_CUT;

    imp::roiCut.x = conf::X_ROI;
    imp::roiCut.y = conf::Y_ROI;
    imp::roiCut.width = conf::W_ROI;
    imp::roiCut.height = conf::H_ROI;

    getMatrixWrap(imp::matrixWrap, conf::W_CUT, conf::H_CUT, imp::ALPHA,
                  imp::F, imp::DIST, (conf::W_ROI - conf::W_CUT)/2, imp::Y, imp::SCALE_X);

    imp::inverMatrixWrap = imp::matrixWrap.clone().inv();
}

void getMatrixWrap(cv::Mat &dst, double w, double h, double alpha,
                   double focalLength, double dist, double x, double y, double scale_x){

    double PI = 3.1415926;

    alpha = (alpha/10 - 90) * PI/180;
    double beta = 0;
    double gamma = 0;


    // Projecion matrix 2D -> 3D
    cv::Mat A1 = (cv::Mat_<float>(4, 3)<<
                  1, 0, -w/2,
                  0, 1, -h/2,
                  0, 0, 0,
                  0, 0, 1 );


    // Rotation matrices Rx, Ry, Rz

    cv::Mat RX = (cv::Mat_<float>(4, 4) <<
                  1, 0, 0, 0,
                  0, cos(alpha), -sin(alpha), 0,
                  0, sin(alpha), cos(alpha), 0,
                  0, 0, 0, 1 );

    cv::Mat RY = (cv::Mat_<float>(4, 4) <<
                  cos(beta), 0, -sin(beta), 0,
                  0, 1, 0, 0,
                  sin(beta), 0, cos(beta), 0,
                  0, 0, 0, 1	);

    cv::Mat RZ = (cv::Mat_<float>(4, 4) <<
                  cos(gamma), -sin(gamma), 0, 0,
                  sin(gamma), cos(gamma), 0, 0,
                  0, 0, 1, 0,
                  0, 0, 0, 1	);

    cv::Mat RO = (cv::Mat_<float>(4, 4) <<
                  1, 0, 0, -x,
                  0, 1, 0, -y,
                  0, 0, 1, 0,
                  0, 0, 0, 1	);
    float sc = (float)(scale_x)/100;
    cv::Mat RS = (cv::Mat_<float>(4, 4) <<
        sc, 0, 0, 0,
        0, 1, 0, 0,
        0, 0, 1, 0,
        0, 0, 0, 1);


    // R - rotation matrix
    cv::Mat R =  RX * RY * RZ *  RS * RO;

    // T - translation matrix
    cv::Mat T = (cv::Mat_<float>(4, 4) <<
                 1, 0, 0, 0,
                 0, 1, 0, 0,
                 0, 0, 1, dist,
                 0, 0, 0, 1);

    // K - intrinsic matrix
    cv::Mat K = (cv::Mat_<float>(3, 4) <<
                 focalLength, 0, w/2, 0,
                 0, focalLength, h/2, 0,
                 0, 0, 1, 0
                 );
    dst = K * (T * (R * A1));

}
