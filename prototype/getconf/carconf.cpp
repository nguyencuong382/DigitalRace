#include "carconf.h"

namespace car{
    int SPEED = 30;

    int ALP;
    int BET;
    int GAM;
    int DEL;
    int MAX_SPD;
    int GAM_SPD;
    int DEL_SPD;
    int MIN_SPD;
    int INJECT_SPD;
    int OBJ_SPD;

    int GAM_BET;
    int DEL_GAM;

    int MAX_GAM_SPD;
    int GAM_SPD_DEL_SPD;

    int LOST_LANE_SPD;

    double START_SPD;
    double STEP_START_SPD;
    int STOP_SPD;
    int STEP_STOP_SPD;

}

namespace refer {
    double A = 200;
    double O = 0;
    double B = -200;
    double a = 22;
    double b = -22;
}


void initCar(Config &getConfigs){
    // set referecences
    refer::A = getConfigs.find("A");
    refer::O = getConfigs.find("O");
    refer::B = getConfigs.find("B");
    refer::a = getConfigs.find("a");
    refer::b = getConfigs.find("b");

    car::ALP = getConfigs.find("ALP");
    car::BET = getConfigs.find("BET");
    car::GAM = getConfigs.find("GAM");
    car::DEL = getConfigs.find("DEL");
    car::INJECT_SPD = getConfigs.find("INJECT_SPD");
    car::OBJ_SPD = getConfigs.find("OBJ_SPD");

    car::MAX_SPD = getConfigs.find("MAX_SPD");
    car::GAM_SPD = getConfigs.find("GAM_SPD");
    car::DEL_SPD = getConfigs.find("DEL_SPD");
    car::MIN_SPD = getConfigs.find("MIN_SPD");

    car::GAM_BET = car::GAM - car::BET;
    car::DEL_GAM = car::DEL - car::GAM;

    car::MAX_GAM_SPD = car::MAX_SPD - car::GAM_SPD;
    car::GAM_SPD_DEL_SPD = car::GAM_SPD - car::DEL_SPD;

    car::LOST_LANE_SPD = getConfigs.find("LOST_LANE_SPD");


    car::START_SPD = (double)getConfigs.find("START_SPD");
    car::STEP_START_SPD= (double)getConfigs.find("STEP_START_SPD") / 10;
    car::STOP_SPD = getConfigs.find("STOP_SPD");
    car::STEP_STOP_SPD = getConfigs.find("STEP_STOP_SPD");

}
