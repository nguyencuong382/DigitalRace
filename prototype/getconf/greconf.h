#ifndef GREEN_H
#define GREEN_H

#include <opencv2/highgui/highgui.hpp>
#include "readparam.h"

namespace gre {
    extern int L_GREEN;
    extern int H_GREEN;
    extern double X_RESIZE_RATIO;
    extern double Y_RESIZE_RATIO;


    // config for image processing
    extern int THRES;
    extern int NUMLAYERS;
    extern int NUM_SUB_LAYERS;
    extern int MIN_AREA_CNT;
    extern int MIN_AREA_GREEN_CNT;
    extern int MIN_WIDTH_CNT;
    extern double RATIO_CNT_CONVEXHULL;

    extern int MAX_DIS_TWO_POINTS;
    extern int MIN_POINTS;
    extern int MIN_POINTS_AFTER_COMBINE;
    extern int MAX_GROUP_POINTS;
    extern int NUM_AVERAGE_POINTS;

    extern int H_LAYER;
    extern int W_SUB_LAYER;


    // distance between Intersection and lowest line, use for prosseing road injection
    extern int DIS_ROAD_INJUNCTION;
    extern int ANGLE_LINES_INJECTION;
    extern double SLOPE_MAX_LINE_INJUNCTION;
    extern int DOWN_Y_INJUNCTION;

    // speed of car
    extern int DEBUG;
    extern int WAIT_KEY;

    extern int H_ROI;
    extern int W_ROI;

    extern int H_LOW;
    extern int S_LOW;
    extern int V_LOW;

    extern int H_HIGH;
    extern int S_HIGH;
    extern int V_HIGH;

    extern int DILATION_SIZE;
    extern int EROSION_SIZE;
    extern int SCALE_SCHARR;
    extern int OFF_SET_CUT_X_OBJ;
    extern int OFF_SET_CUT_Y_OBJ;
    extern int NUM_KERNEL_MORP;

    extern std::vector<cv::Mat> kernels_morp;
    extern cv::Mat kernel_gradient;
    extern cv::Rect roi_obj;

    extern cv::Scalar lowHSV;
    extern cv::Scalar highHSV;
    extern std::vector<std::vector<cv::Point>> co_ordinates;
    extern cv::Mat mask_roi;

    extern cv::Scalar white;
    extern cv::Rect roi_mask;
    extern std::vector<std::vector< std::vector<int>>> grid;

    extern std::vector<cv::Vec3b> _colors_;
    extern cv::Vec3b _black;
    extern cv::Vec3b _white;
}

void initGreen(Config &getConfigs);

#endif // GREEN_H

