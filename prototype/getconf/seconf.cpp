//
// Created by nc on 11/05/2018.
//

#include "seconf.h"

namespace se {
    int MAX_DIS_TWO_POINTS = 30;
    int MIN_POINTS = 5;
    int MIN_POINTS_AFTER_COMBINE;

    // distance between Intersection and lowest line, use for prosseing road injection
    int DIS_ROAD_INJUNCTION = 150;
    int ANGLE_LINES_INJECTION = 60;
    double SLOPE_MAX_LINE_INJUNCTION = 4;
    int DOWN_Y_INJUNCTION = 100;
    double RATIO_POINTS_LR;
    int MAX_ANGLE_TWO_LANES;
    int MIN_LEN_LANE;
    int DOWN_Y;
    // speed of car
    int DEBUG = 1;
    int OFF_SET_CUT_X_OBJ = 2;
    int OFF_SET_CUT_Y_OBJ = 2;
    cv::Rect roi_obj;
}



void initSe(Config &getConfigs){

    se::MAX_DIS_TWO_POINTS = getConfigs.find("MAX_DIS_TWO_POINTS");
    se::MIN_POINTS = getConfigs.find("MIN_POINTS");
    se::MIN_POINTS_AFTER_COMBINE = getConfigs.find("MIN_POINTS_AFTER_COMBINE");

    se::DIS_ROAD_INJUNCTION = getConfigs.find("DIS_ROAD_INJUNCTION");
    se::ANGLE_LINES_INJECTION = getConfigs.find("ANGLE_LINES_INJECTION");
    se::SLOPE_MAX_LINE_INJUNCTION = (double)getConfigs.find("SLOPE_MAX_LINE_INJUNCTION")/10;
    se::DOWN_Y_INJUNCTION = getConfigs.find("DOWN_Y_INJUNCTION");
    se::MAX_ANGLE_TWO_LANES = getConfigs.find("MAX_ANGLE_TWO_LANES");
    se::MIN_LEN_LANE = getConfigs.find("MIN_LEN_LANE");

    se::DEBUG = getConfigs.find("DEBUG");
    se::DOWN_Y = getConfigs.find("DOWN_Y");
    se::RATIO_POINTS_LR = (double)getConfigs.find("RATIO_POINTS_LR") / 10;



    se::OFF_SET_CUT_X_OBJ = getConfigs.find("OFF_SET_CUT_X_OBJ");
    se::OFF_SET_CUT_Y_OBJ = getConfigs.find("OFF_SET_CUT_Y_OBJ");

    se::roi_obj = cv::Rect(se::OFF_SET_CUT_X_OBJ,
                            se::OFF_SET_CUT_Y_OBJ,
                            conf::W_CUT  - 2 * se::OFF_SET_CUT_X_OBJ,
                            conf::H_CUT - 2 * se::OFF_SET_CUT_Y_OBJ);

}