#include "objconf.h"
namespace obj{
    int TYPE = 1;
    int MIN_AREA = 90;
    int CHECK_COLOR = 1;
    int DEBUG = 0;
    int ROI_FLOOR_DIV= 2;
    int ROI_DIV_Y = 3;
    int ROI_WIDTH_DOWN = 20;
    int ROI_X = 5;
    int ROI_FLOOR_MIN_DEPTH = 0;
    int ROI_FLOOR_MAX_DEPTH = 0;
    int MIN_S = 150;
    int MAX_S = 255;
    int MIN_V = 50;
    int MAX_V = 150;
}

void initObject(Config &getConfigs){
    obj::MIN_S = getConfigs.find("MIN_S");
    obj::MAX_S= getConfigs.find("MAX_S");
    obj::MIN_V=getConfigs.find("MIN_V");
    obj::MAX_V = getConfigs.find("MAX_V");

    obj::TYPE = getConfigs.find("TYPE");
    obj::ROI_X = getConfigs.find("ROI_X");
    obj::ROI_DIV_Y = getConfigs.find("ROI_DIV_Y");
    obj::ROI_FLOOR_DIV =getConfigs.find("ROI_FLOOR_DIV");
    obj::CHECK_COLOR = getConfigs.find("CHECK_COLOR");
    obj::MIN_AREA = getConfigs.find("MIN_AREA");
    obj::DEBUG = getConfigs.find("DEBUG");
    obj::ROI_FLOOR_MIN_DEPTH = getConfigs.find("ROI_FLOOR_MIN_DEPTH");
    obj::ROI_FLOOR_MAX_DEPTH = getConfigs.find("ROI_FLOOR_MAX_DEPTH");
    obj::ROI_WIDTH_DOWN = getConfigs.find("ROI_WIDTH_DOWN");
}
