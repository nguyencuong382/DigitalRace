#include "readparam.h"

void Config::readlines(){
    std::ifstream infile(this->path);

    if(infile.is_open()){
        std::cout << "Reading... " << path << std::endl;
        std::string line;
        while (std::getline(infile, line))
        {
            int specialChar = 0;
            for(char c : line){
                if(c == '/'){
                    specialChar++;
                    break;
                }
            }

            if(specialChar == 0){

                split(line);
            }

        }
    } else {
        std::cout << "Failed open file config... " << path <<std::endl;
    }

}

void Config::split(std::string t){

    std::stringstream test(t);
    std::string segment;
    std::vector<std::string> object;

    while (std::getline(test, segment, '='))
    {
        segment.erase(std::remove(segment.begin(), segment.end(), ' '), segment.end());
        object.push_back(segment);
    }

    for (unsigned int i = 0; i < object.size(); i += 2)
    {
        int value = std::stoi(object[i + 1]);
        this->items.insert(std::pair<std::string, int>(object[i], value));

        std::cout << object[i] <<  " = " << value << std::endl;

    }

}

void Config::setpath(std::string t){
    this->path = t;
    this->items.clear();
    readlines();
}

int Config::find(std::string t){

    auto it = this->items.find(t);
    if(it != items.end()){
        return it->second;
    } else {
        std::cout <<"---------WARNING----------- : can't not read param: " << t << std::endl;
        return -1;
    }
}
