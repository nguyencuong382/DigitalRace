//
// Created by nc on 11/05/2018.
//

#ifndef CDS_SECONF_H
#define CDS_SECONF_H

#include <opencv2/highgui/highgui.hpp>
#include "readparam.h"
#include "config.h"
#include "greconf.h"

namespace se {
    extern int MIN_POINTS;
    extern int MAX_DIS_TWO_POINTS;
    extern int MIN_POINTS_AFTER_COMBINE;

    extern int DIS_ROAD_INJUNCTION;
    extern int ANGLE_LINES_INJECTION;
    extern double SLOPE_MAX_LINE_INJUNCTION;
    extern int DOWN_Y_INJUNCTION;

    extern int MAX_ANGLE_TWO_LANES;
    extern int MIN_LEN_LANE;
    extern int DOWN_Y;
    extern double RATIO_POINTS_LR;
    // speed of car
    extern int DEBUG;

    extern int OFF_SET_CUT_X_OBJ;
    extern int OFF_SET_CUT_Y_OBJ;
    extern cv::Rect roi_obj;
}

void initSe(Config &getConfigs);

#endif //CDS_SECONF_H
