#ifndef OBJCONF_H
#define OBJCONF_H
#include "readparam.h"
namespace obj{
    extern int TYPE;
	extern int MIN_AREA;
extern int CHECK_COLOR;
extern int DEBUG;
extern int ROI_FLOOR_DIV;
extern int ROI_DIV_Y;
extern int ROI_WIDTH_DOWN;
extern int ROI_X;
extern int ROI_FLOOR_MIN_DEPTH;
extern int ROI_FLOOR_MAX_DEPTH;
extern int MIN_S;
extern int MIN_V;
extern int MAX_S;
extern int MAX_V;

}
void initObject(Config &getConfigs);

#endif // OBJCONF_H
