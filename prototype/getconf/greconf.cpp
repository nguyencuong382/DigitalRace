#include "greconf.h"
#include "config.h"
#include <iostream>
#include "opencv2/imgproc/imgproc.hpp"

namespace gre {
    int L_GREEN;
    int H_GREEN;

    double X_RESIZE_RATIO;
    double Y_RESIZE_RATIO;

    // config for image processing
    int THRES = 200;
    int NUMLAYERS = 25;
    int NUM_SUB_LAYERS = 50;
    int H_LAYER;
    int W_SUB_LAYER;

    int MIN_AREA_CNT = 20;
    int MIN_AREA_GREEN_CNT = 5000;
    int MIN_WIDTH_CNT = 20;
    double RATIO_CNT_CONVEXHULL = 0.8;

    int MAX_DIS_TWO_POINTS = 30;
    int MIN_POINTS = 5;
    int MIN_POINTS_AFTER_COMBINE = 5;
    int MAX_GROUP_POINTS = 25;
    int NUM_AVERAGE_POINTS = 2;

    // distance between Intersection and lowest line, use for prosseing road injection
    int DIS_ROAD_INJUNCTION = 150;
    int ANGLE_LINES_INJECTION = 60;
    double SLOPE_MAX_LINE_INJUNCTION = 4;
    int DOWN_Y_INJUNCTION = 100;

    int H_ROI;
    int W_ROI;


    // speed of car
    int DEBUG = 1;
    int WAIT_KEY = 1 ;

    int H_LOW = 60;
    int S_LOW = 105;
    int V_LOW = 83;

    int H_HIGH = 255;
    int S_HIGH = 255;
    int V_HIGH = 255;


    int DILATION_SIZE = 2;
    int EROSION_SIZE = 3;
    int SCALE_SCHARR = 1;
    int OFF_SET_CUT_X_OBJ = 2;
    int OFF_SET_CUT_Y_OBJ = 2;
    int NUM_KERNEL_MORP = 1;

    cv::Mat kernel_gradient;
    std::vector<cv::Mat> kernels_morp;
    cv::Rect roi_obj;

    cv::Scalar lowHSV(H_LOW, S_LOW, V_LOW);
    cv::Scalar highHSV(H_HIGH, S_HIGH, V_HIGH);

    std::vector<std::vector<cv::Point>> co_ordinates;
    cv::Mat mask_roi;

    cv::Scalar white(255);
    cv::Rect roi_mask;

    std::vector<std::vector< std::vector<int>>> grid;

    std::vector<cv::Vec3b> _colors_;
    cv::Vec3b _black(0, 0, 0);
    cv::Vec3b _white(0, 0, 255);
}



void initGreen(Config &getConfigs){
    gre::L_GREEN = getConfigs.find("L_GREEN");
    gre::H_GREEN = getConfigs.find("H_GREEN");

        // config for image processing
    gre::THRES = getConfigs.find("THRES");
    gre::NUMLAYERS = getConfigs.find("NUMLAYERS");
    gre::NUM_SUB_LAYERS = getConfigs.find("NUM_SUB_LAYERS");

    gre::MIN_AREA_CNT = getConfigs.find("MIN_AREA_CNT");
    gre::MIN_AREA_GREEN_CNT = getConfigs.find("MIN_AREA_GREEN_CNT");
    gre::MIN_WIDTH_CNT = getConfigs.find("MIN_WIDTH_CNT");
    gre::RATIO_CNT_CONVEXHULL = (double)getConfigs.find("RATIO_CNT_CONVEXHULL")/100.0;


    gre::MAX_DIS_TWO_POINTS = getConfigs.find("MAX_DIS_TWO_POINTS");
    gre::MIN_POINTS = getConfigs.find("MIN_POINTS");
    gre::MIN_POINTS_AFTER_COMBINE = getConfigs.find("MIN_POINTS_AFTER_COMBINE");
    gre::MAX_GROUP_POINTS = getConfigs.find("MAX_GROUP_POINTS");
    gre::NUM_AVERAGE_POINTS = getConfigs.find("NUM_AVERAGE_POINTS");

    gre::DIS_ROAD_INJUNCTION = getConfigs.find("DIS_ROAD_INJUNCTION");
    gre::ANGLE_LINES_INJECTION = getConfigs.find("ANGLE_LINES_INJECTION");
    gre::SLOPE_MAX_LINE_INJUNCTION = (double)getConfigs.find("SLOPE_MAX_LINE_INJUNCTION")/10;
    gre::DOWN_Y_INJUNCTION = getConfigs.find("DOWN_Y_INJUNCTION");

    gre::H_ROI = getConfigs.find("H_ROI");
    gre::W_ROI = getConfigs.find("W_ROI");

    gre::DEBUG = getConfigs.find("DEBUG");
    gre::WAIT_KEY = getConfigs.find("WAIT_KEY");

    gre::DILATION_SIZE = getConfigs.find("DILATION_SIZE");
    gre::EROSION_SIZE = getConfigs.find("EROSION_SIZE");
    gre::SCALE_SCHARR = getConfigs.find("SCALE_SCHARR");
    gre::OFF_SET_CUT_X_OBJ = getConfigs.find("OFF_SET_CUT_X_OBJ");
    gre::OFF_SET_CUT_Y_OBJ = getConfigs.find("OFF_SET_CUT_Y_OBJ");
    gre::NUM_KERNEL_MORP = getConfigs.find("NUM_KERNEL_MORP");

    for(int r = 1; r < gre::NUM_KERNEL_MORP + 1; r++){
        cv::Mat kernel = cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(2*r+1, 2*r+1));
        gre::kernels_morp.push_back(kernel);
    }

    gre::kernel_gradient = cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(7, 7));

    gre::roi_obj = cv::Rect(gre::OFF_SET_CUT_X_OBJ,
                            gre::OFF_SET_CUT_Y_OBJ,
                            conf::W_CUT  - 2 * gre::OFF_SET_CUT_X_OBJ,
                            conf::H_CUT - 2*gre::OFF_SET_CUT_Y_OBJ);

    gre::H_LOW = getConfigs.find("H_LOW");
    gre::S_LOW = getConfigs.find("S_LOW");
    gre::V_LOW = getConfigs.find("V_LOW");

    gre::H_HIGH = getConfigs.find("H_HIGH");
    gre::S_HIGH = getConfigs.find("S_HIGH");
    gre::V_HIGH = getConfigs.find("V_HIGH");

    gre::lowHSV  = cv::Scalar(gre::H_LOW, gre::S_LOW, gre::V_LOW);
    gre::highHSV = cv::Scalar(gre::H_HIGH, gre::S_HIGH, gre::V_HIGH);

    gre::co_ordinates.push_back(std::vector<cv::Point>());

    gre::co_ordinates[0].push_back(cv::Point());
    gre::co_ordinates[0].push_back(cv::Point(0,100));
    gre::co_ordinates[0].push_back(cv::Point(100, conf::H_ROI));
    gre::co_ordinates[0].push_back(cv::Point(conf::W_ROI- 100, conf::H_ROI));
    gre::co_ordinates[0].push_back(cv::Point(conf::W_ROI,100));
    gre::co_ordinates[0].push_back(cv::Point(conf::W_ROI, 0));

    gre::mask_roi = cv::Mat(conf::H_ROI, conf::W_ROI, CV_8UC1, cv::Scalar(0));
    cv::drawContours(gre::mask_roi,gre::co_ordinates,0, cv::Scalar(255),CV_FILLED, 8 );
//    cv::ellipse( gre::mask_roi, cv::Point( gre::W_ROI/2, gre::H_ROI/2 ), cv::Size(gre::W_ROI/2, gre::H_ROI/2 ), 0, 0, 360, cv::Scalar( 255, 255, 255 ), -1, 8 );

    gre::X_RESIZE_RATIO = (double)conf::W_CUT/(double)gre::W_ROI;
    gre::Y_RESIZE_RATIO = (double)conf::H_CUT/(double)gre::H_ROI;

    gre::H_LAYER = gre::H_ROI / gre::NUMLAYERS;
    gre::W_SUB_LAYER = gre::W_ROI / gre::NUM_SUB_LAYERS;

    int dy = (gre::H_ROI - gre::NUMLAYERS * gre::H_LAYER)/gre::H_LAYER + 2;
    int dx = (gre::W_ROI - gre::NUM_SUB_LAYERS * gre::W_SUB_LAYER)/gre::W_SUB_LAYER + 10;

    gre::NUM_SUB_LAYERS += dx;
    gre::NUMLAYERS += dy;


    std::vector<int> d1(3);
    std::vector<std::vector<int>> d2(gre::NUM_SUB_LAYERS, d1);
    gre::grid = std::vector<std::vector< std::vector<int>>>(gre::NUMLAYERS, d2);

    int off_set = 5;
    gre::roi_mask = cv::Rect(off_set,off_set, gre::W_ROI - 2 * off_set, gre::H_ROI - 2 * off_set);



    gre::_colors_.push_back(cv::Vec3b(120, 255, 255));
    gre::_colors_.push_back(cv::Vec3b(60, 255, 255));
    gre::_colors_.push_back(cv::Vec3b(0, 255, 255));

}