#ifndef TRACONF_H
#define TRACONF_H

#include "readparam.h"
#include "opencv2/highgui/highgui.hpp"

namespace tra{
    extern int WIDTH;
    extern int HEIGHT;

    extern int DEBUG;
    extern int W_RESIZE;
    extern int H_RESIZE;
    extern int RESIZE_RATIO;
    extern double INTERS_AREA_RATIO;

    extern int X_CUT;
    extern int Y_CUT;
    extern int Y_BOT_OFFSET;
    extern int W_CUT;
    extern int H_CUT;

    extern int MIN_CNT;
    extern int MAX_CNT;

    extern double H_RATIO;
    extern double L_RATIO;

    extern int H_LOW;
    extern int S_LOW;
    extern int V_LOW;

    extern int H_HIGH;
    extern int S_HIGH;
    extern int V_HIGH;


    extern cv::Scalar lowHSV;
    extern cv::Scalar highHSV;

    extern cv::Rect roi;
}

void initTraffic(Config &getConfigs);

#endif // TRACONF_H
