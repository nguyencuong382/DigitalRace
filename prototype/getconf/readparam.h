#ifndef READPARAM_H
#define READPARAM_H

#include <iostream>
#include <vector>
#include <map>
#include <fstream>
#include <string>
#include <sstream>
#include <algorithm>
#include <unistd.h>

class Config{
private:
    std::string path;
    std::map<std::string, int> items;
    void readlines();

    void split(std::string);

public:
    void setpath(std::string);

    int find(std::string);

};

#endif // READPARAM_H
