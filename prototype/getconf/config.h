#ifndef CONFIG_H
#define CONFIG_H

#include <opencv2/highgui/highgui.hpp>
#include "readparam.h"

namespace conf {
    extern int WIDTH;
    extern int HEIGHT;

    // Region of lane
    extern int X_CUT;
    extern int Y_CUT;
    extern int Y_BOT_OFFSET;
    extern int W_CUT;
    extern int H_CUT;

    // Region of birdview which will be taked from region of lane
    extern int W_ROI;
    extern int X_ROI;
    extern int Y_ROI;
    extern int H_ROI;


    // config for image processing
    extern int THRES;
    extern int NUMLAYERS;
    extern int MIN_AREA_CNT;
    extern int MIN_WIDTH_CNT;

    extern int DOWN_Y;
    extern int DOWN_Y_INJEC;
    extern int MIN_DIS_CAR_OBJ;
    extern int MAX_DIS_CAR_OBJ;
    extern int DIS_TURN_OBJ;
    extern int CLOSE_PROXIMITY_CAR_OBJ;

    extern int DOWN_Y_OBJ;
    extern int DOWN_Y_FAR;
    extern int DOWN_Y_LOST_LANE;
    extern int MAX_ANGLE_COMBINE_LANES;
    extern int MAX_X_DIS_COMBINE_LINES;
    extern int MAX_ANGLE_TWO_LANES;

    // Region of layer
    extern int H_LAYER;

    extern int MAX_DIS_TWO_POINTS;
    extern int MIN_POINTS;
    extern int MIN_POINTS_AFTER_COMBINE;
    extern int MAX_ANGLE_TWO_POINTS;
    extern int FOLLOW_LANE;
    // Distace between two line, use for processing one lane;
    extern int DIS_GEN_LINE_TOP;
    extern int DIS_GEN_LINE_BOT;

    // distance between Intersection and lowest line, use for prosseing road injection
    extern int DIS_ROAD_INJUNCTION;
    extern int SLOPE_MAX_LINE_INJUNCTION;
    extern int MIN_X_INJUNCTION;
    extern int DIS_TOP_INJ;
    extern int DIS_BOT_INJ;
    extern int MIN_AREA_WHITE;

    extern double RATIO_POINTS_LR;

    extern int DEBUG;
    extern int WAIT_KEY;
    extern int WRITE_VIDEO;

    extern int MAX_DIS_BOT;

    extern int DIS_FAR_CAR;
    extern int DIS_NEXT_CAR;
    extern int MIN_LEN_LANE;
    extern int DIS_EXTERNAL;

}

namespace imp {
    extern int ALPHA;
    extern int F;
    extern int DIST;
    extern int Y;
    extern int SCALE_X;
    extern int X;

    extern cv::Rect roi;
    extern cv::Rect roiCut;
    extern cv::Mat matrixWrap;
    extern cv::Mat inverMatrixWrap;
}


void initLane(Config &getConfigs);


void getMatrixWrap(cv::Mat &dst, double w, double h, double alpha,
                   double focalLength, double dist, double x, double y, double scale_x);

#endif // CONFIG_H

