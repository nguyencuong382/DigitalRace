#include "detectorobj.h"
#include "iostream"


cv::Rect roi;
cv::Mat element = cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(5, 5));
std::vector<cv::Vec3b> colors;
int min_s = 120, min_v = 50;
cv::Vec3b black(0, 0, 0);
cv::Vec3b white(0, 0, 255);

Detector_Obj::Detector_Obj()
{
    roi.x = 5;
    roi.y = VIDEO_FRAME_HEIGHT / 3;
    roi.width = VIDEO_FRAME_WIDTH - 20;
    roi.height = VIDEO_FRAME_HEIGHT - roi.y;
    colors.push_back(cv::Vec3b(120, 255, 255));
    colors.push_back(cv::Vec3b(60, 255, 255));
    colors.push_back(cv::Vec3b(0, 255, 255));
}

void get_closest(cv::Vec3b &pixcel)
{
    int min = 255;
    if (pixcel[1] < min_s)
    {
	   pixcel = white;
	   return;
    }
    if (pixcel[1] < min_v)
    {
	   pixcel = black;
	   return;
    }
    for (auto color : colors)
    {
	   int distance = cv::abs(pixcel[0] - color[0]);
	   if (distance < min)
	   {
		  min = distance;

		  if (color[0] == 120)//select only red
		  {
			 pixcel = color;
		  }
		  else
		  {
			 pixcel = white;
		  }
	   }
    }
}

bool get_mask_normaled_color(cv::Mat &src, cv::Mat &dst,cv::RotatedRect &minRect) {
    bool is_object = false;
    cv::cvtColor(src, dst, cv::COLOR_RGB2HSV);
    for (int y = 0; y < dst.rows; ++y) {
	   for (int x = 0; x < dst.cols; ++x) {
		  cv::Vec3b  current = dst.at<cv::Vec3b>(y, x);
		  get_closest(current);
		  dst.at<cv::Vec3b>(y, x) = current;
	   }
    }

    //DEBUG
//    cv::Mat rgb;
//    cv::cvtColor(src, rgb, cv::COLOR_HSV2RGB);
//    cv::imshow("color_normed", rgb);
    cv::Mat gray;
    cv::Mat hsv_channels[3];
    cv::split(dst, hsv_channels);
    gray = hsv_channels[1];
    std::vector<std::vector<cv::Point> > contours;
    cv::findContours(gray, contours, cv::RETR_CCOMP, cv::CHAIN_APPROX_SIMPLE);

    dst = cv::Mat::zeros(gray.size(), CV_8UC1);
    std::vector<std::vector<cv::Point> > hull;
    for (int i = 0; i < contours.size(); i++) {
	   if (cv::contourArea(contours[i]) > 100) {
		  std::vector<cv::Point> sub_hull;
		  cv::convexHull(cv::Mat(contours[i]), sub_hull, false);
		  hull.push_back(sub_hull);
		  minRect = minAreaRect(sub_hull);
		  is_object = true;
	   }
    }


//        find_if_contours_cloes(hull);
   
    //drawContours(dst, hull, -1, 255, 1);

    return is_object;

   

}
cv::Rect get_color_in_box(cv::Mat color, cv::Rect maxsize_rect, cv::Vec3b &min, cv::Vec3b &max)
{

    min = cv::Vec3b(255, 255, 255);
    max = cv::Vec3b(0, 0, 0);
    cv::Vec3b current;
    //cv::imshow("clor", color);
    int w = maxsize_rect.width * 0.1, h = maxsize_rect.height*0.1;
    int x = maxsize_rect.x + maxsize_rect.width / 2, y = maxsize_rect.y + maxsize_rect.height / 2;

    //std::cout <<"X Y :"<< maxsize_rect <<"\n";

    if (y + h > color.size().height)
    {
	   h = color.size().height - y;
    }
    if (x + w > color.size().width)
    {
	   w = color.size().width - x;
    }
    for (int i = y; i < y + h; ++i)
    {
	   for (int j = x; j < x + w; ++j)
	   {
		  current = color.at<cv::Vec3b>(i, j);
		  for (int t = 0; t < 3; ++t)
		  {
			 if (current[t] > max[t])
			 {
				max[t] = current[t];
			 }
			 if (current[t] < min[t])
			 {
				min[t] = current[t];
			 }
		  }
	   }
    }
    return cv::Rect(x, y, w, h);
}

cv::Rect Detector_Obj::get_color_rect(cv::Mat color, cv::Rect maxsize_rect, int maxsize)
{
    cv::Rect max_color_rect;
    cv::Mat color_clone;
    if (maxsize > 0)
    {
	   color_clone = color.clone();

	   if (maxsize_rect.width + maxsize_rect.x > color.size().width)
	   {
		  maxsize_rect.width = color.size().width - maxsize_rect.x;
	   }
	   if (maxsize_rect.height + maxsize_rect.y > color.size().height)
	   {
		  maxsize_rect.height = color.size().height - maxsize_rect.y;
	   }

	   cv::Mat binary;
	   cv::Vec3b min, max;
	   cv::Rect small_rect = get_color_in_box(color_clone, maxsize_rect, min, max);
	   cv::inRange(color_clone, min, max, binary);
	   //	     std::cout<<min << "\t"<<max <<"\n";
	   //		cv::imshow("clolrcln",binary);
	   cv::blur(binary, binary, cv::Size(3, 3));

	   std::vector<std::vector<cv::Point>> contours;
	   cv::findContours(binary, contours, CV_RETR_TREE, cv::CHAIN_APPROX_SIMPLE);
	   int max_color_rect_size = 0, area;
	   cv::Rect bounding_rect;
	   std::vector<cv::Point> contours_poly;

	   for (size_t i = 0; i < contours.size(); i++) {
		  cv::approxPolyDP(contours[i], contours_poly, 3, false);
		  bounding_rect = cv::boundingRect(contours_poly);
		  //int distance = std::sqrt(pow(bounding_rect.x - maxsize_rect.x, 2) + pow(bounding_rect.y - maxsize_rect.y, 2));
		  bool is_bounding = (bounding_rect.tl().x < small_rect.tl().x)
			 && (bounding_rect.tl().y < small_rect.tl().y)
			 && (bounding_rect.br().x > small_rect.br().x)
			 && (bounding_rect.br().y > small_rect.br().y);
		  area = bounding_rect.area();
		  if (area > max_color_rect_size &&area < 4000 && is_bounding)
		  {
			 max_color_rect_size = area;
			 max_color_rect = bounding_rect;
		  }
	   }
    }
    return max_color_rect;
}

bool Detector_Obj::type_roi_floor(cv::Mat depthImg, cv::Mat color, cv::Rect& object_rect)
{
    cv::resize(color, color, cv::Size(160, 120));
    cv::resize(depthImg, depthImg, cv::Size(160, 120));

    cv::Mat src = depthImg(roi);
    //    cv::blur(src, src, cv::Size(3, 3));
    int w = src.size().width, h = src.size().height;

    cv::Mat binary;
    cv::inRange(src, cv::Scalar(67), cv::Scalar(83), binary);
    //    cv::threshold(depthImg, binary, 70, 72, cv::THRESH_BINARY);
    cv::rectangle(binary, cv::Point(0, h / 5), cv::Point(w, h), cv::Scalar(0), -1);

    std::vector<std::vector<cv::Point>> contours;
    cv::findContours(binary, contours, CV_RETR_TREE, cv::CHAIN_APPROX_SIMPLE);
    cv::Rect bounding_rect;
    cv::blur(binary, binary, cv::Size(3, 3));
    int maxsize = 0;
    for (size_t i = 0; i < contours.size(); i++) {
	   std::vector<cv::Point> contours_poly;
	   cv::approxPolyDP(contours[i], contours_poly, 3, false);
	   bounding_rect = boundingRect(contours_poly);
	   int area = bounding_rect.area();
	   if (area > 500)
	   {
		  if (bounding_rect.width / bounding_rect.height < 3) {
			 bounding_rect.y += roi.y;
			 bounding_rect.x += roi.x;
			 if (area > maxsize)
			 {
				object_rect = bounding_rect;
				maxsize = area;
			 }
			 cv::rectangle(depthImg, bounding_rect, cv::Scalar(255));
		  }
	   }
    }
    object_rect = get_color_rect(color, object_rect, maxsize);
    cv::imshow("binary", binary);
    if (maxsize > 0)
    {
	   rectangle(color, object_rect, cv::Scalar(255, 255, 0));
	   cv::imshow("depthcolor", color);

	   return true;
    }




    return false;
}

bool Detector_Obj::get_object_depth(cv::Mat depthImg, cv::Mat color, cv::Point2f object_rect[])
{
    cv::Mat normed;
    cv::RotatedRect object;
	bool is_object = get_mask_normaled_color(color, normed, object);
//    cv::cvtColor(normed, normed, cv::COLOR_HSV2RGB);
	if (is_object)
	{
//	    cv::Point2f rect_points[4];
	    object.points(object_rect);
//	    object_rect = rect_points;
	    for (int j = 0; j < 4; j++)
		   cv::line(color, object_rect[j], object_rect[(j + 1) % 4], cv::Scalar(255, 255, 0), 1, 8);


	    cv::imshow("color", color);
	}
   

    return true;
}

cv::Mat mask_amaterasu(cv::Mat depth)
{
    cv::Mat binary;
    cv::inRange(depth, cv::Scalar(39), cv::Scalar(39), binary);
    return  binary;
}
bool Detector_Obj::get_two_bottom_points_depth(cv::Mat depthImg, cv::Mat color, cv::Rect &object_rect, std::vector<cv::Rect> &objects)
{
    cv::resize(color, color, cv::Size(160, 120));
    cv::resize(depthImg, depthImg, cv::Size(160, 120));


    objects = get_objects(depthImg);

    // move box
    //cv::Rect maxsize_rect;
    int maxsize = 0, area;

    for (size_t i = 0; i < objects.size(); i++) {
	   area = objects[i].area();
	   if (area > maxsize)
	   {
		  maxsize = area;
		  object_rect = objects[i];
	   }
    }
    //object_rect = get_color_rect(color, object_rect, maxsize);
    object_rect.height = object_rect.width;
    if (maxsize > 0) {
	   //rectangle(color, object_rect, cv::Scalar(255, 255, 0));
	   //imshow("depthcolor",color);
	   return true;
    }
    return false;
}

bool is_no_depth(cv::Mat depth, cv::Rect rect)
{
    int value = depth.at<char>(rect.y + rect.height / 2, rect.x + rect.width / 2);
    return value == 39;

}

std::vector<cv::Rect> Detector_Obj::get_objects(cv::Mat depth)
{
    std::vector<std::vector<cv::Point>> contours;
    cv::Mat src = depth(roi);

    cv::Mat amaterasu_mask = mask_amaterasu(src);


    cv::Canny(src, src, 100, 255);

    cv::bitwise_and(src, src, amaterasu_mask);
    imshow("dep", src);


    cv::findContours(src, contours, CV_RETR_TREE, cv::CHAIN_APPROX_SIMPLE);
    std::vector<cv::Rect> bounding_rects;
    cv::Rect bounding_rect;

    for (size_t i = 0; i < contours.size(); i++) {
	   std::vector<cv::Point> contours_poly;
	   cv::approxPolyDP(contours[i], contours_poly, 3, false);
	   bounding_rect = boundingRect(contours_poly);

	   is_no_depth(depth, bounding_rect);
	   if (bounding_rect.width*bounding_rect.height > 500)
	   {
		  if (bounding_rect.width / bounding_rect.height < 3 && !is_no_depth(depth, bounding_rect)) {
			 bounding_rect.y += roi.y;
			 bounding_rect.x += roi.x;
			 bounding_rects.push_back(bounding_rect);
		  }
	   }
    }

    //cv::imshow("obje1ct", src);


    return bounding_rects;
}




namespace objdetect {
    Detector_Obj detector;
}
