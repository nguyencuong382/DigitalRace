// OpenCV imports
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

// C++ imports
#include <iostream>
#include <getparam.h>
#include "config.h"
#include <frameprocess.h>
#include "green.h"
#include "opencv2/imgproc/imgproc.hpp"
#include "pointsprocess.h"
#include "laneimageprocess.h"
#include "laneprocess.h"
#include "lineprocess.h"
#include "debug.h"

// namespaces
using namespace std;
using namespace cv;

double freq = getTickFrequency();
double st = 0, et = 0, fps = 0;
double sum_fps = 0;

// VideoWriter video("./out.avi",CV_FOURCC('D','I','V', 'X'),10, cv::Size(320,160),false);

void processLane(cv::Mat src){
    processImg(src);
}

void maskGreen(cv::Mat src){
    cv::Mat hsv;
    cv::cvtColor(src, hsv, cv::COLOR_RGB2HSV);
    std::vector<std::vector<cv::Point>> contours;
    findContoursNormColorHarCode(hsv, contours);
    for (int i = 0; i < contours.size(); i++) {

        if(cv::contourArea(contours[i]) <= gre::MIN_AREA_GREEN_CNT){
            contours.erase(contours.begin() + i);
            i --;
        }
    }
    cv::Mat mask = cv::Mat::zeros(hsv.size(), CV_8UC3);
    cv::drawContours(mask, contours, -1 , cv::Scalar(255), 1 , 8 );
    cv::imshow("mask green", mask);
    cv::imshow("src", src);
}

void process(int track, void *userdata){
    Mat frame = *((Mat*)userdata);
    cv::resize(frame, frame, cv::Size(conf::WIDTH, conf::HEIGHT));

    getMatrixWrap(imp::matrixWrap, conf::W_CUT, conf::H_CUT, imp::ALPHA,
                  imp::F, imp::DIST, (conf::W_ROI - conf::W_CUT)/2, imp::Y, imp::SCALE_X);
    imp::inverMatrixWrap = imp::matrixWrap.clone().inv();
    st = getTickCount();

    processLane(frame);
//    maskGreen(frame);
//

    et = getTickCount();
    sum_fps = 1.0 / ((et-st)/freq);
    cerr << "FPS : "<< sum_fps << '\n';

}

int main() {
	init("/home/nc/Desktop/prototype");
//    VideoCapture capture("/home/nc/Desktop/video/Video1/fd.avi");
    VideoCapture capture("/home/nc/Desktop/video/61.avi");
//
    if( !capture.isOpened()) throw "Error reading video";

	namedWindow("Result", 1);
    Mat source;

    createTrackbar("Alpha", "Result", &imp::ALPHA, 180, process, (void*)(&source));
    createTrackbar("Y", "Result", &imp::Y, 1000 , process, (void*)(&source));
    createTrackbar("Thres", "Result", &conf::THRES, 255, process, (void*)(&source));
    createTrackbar("Thres Gre", "Result", &gre::THRES, 255, process, (void*)(&source));
    createTrackbar("Scle X", "Result", &imp::SCALE_X, 100, process, (void*)(&source));
    createTrackbar("DOWN Y", "Result", &conf::DOWN_Y, conf::H_ROI, process, (void*)(&source));
    createTrackbar("DOWN Y LOST LANE", "Result", &conf::DOWN_Y_LOST_LANE, conf::H_ROI, process, (void*)(&source));
    createTrackbar("MIN WIDTH ", "Result", &conf::MIN_WIDTH_CNT,200, process, (void*)(&source));
    createTrackbar("DIS", "Result", &imp::DIST, 500, process, (void*)(&source));
    createTrackbar("DIS TOP", "Result", &conf::DIS_GEN_LINE_TOP, 255, process, (void*)(&source));
    createTrackbar("DIS BOT", "Result", &conf::DIS_GEN_LINE_BOT, 255, process, (void*)(&source));
    createTrackbar("DIS_EXTERNAL", "Result", &conf::DIS_EXTERNAL, 255, process, (void*)(&source));
    createTrackbar("L G", "Result", &gre::L_GREEN, 255, process, (void*)(&source));
    createTrackbar("H G", "Result", &gre::H_GREEN, 255, process, (void*)(&source));
    createTrackbar("MIN AREA G", "Result", &gre::MIN_AREA_GREEN_CNT, 1000, process, (void*)(&source));


    while( true ) {
        capture >> source;
        if(source.empty()){
            break;
        }
        process(0, (void*)(&source));
        int k = waitKey(conf::WAIT_KEY) & 0xff;
        if(k == 32){
            waitKey();
        }
        if(k == 27){
            break;
        }
	}

    destroyAllWindows();
    capture.release();

	return 0;
}
