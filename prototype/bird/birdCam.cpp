#include "api_kinect_cv.h"
#include "api_i2c_pwm.h"
#include <iostream>
#include "Hal.h"
#include "getparam.h"
#include "config.h"
#include "greenlane.h"

using namespace openni;
#define SAMPLE_READ_WAIT_TIMEOUT 2000 //2000ms
#define VIDEO_FRAME_WIDTH 640	
#define VIDEO_FRAME_HEIGHT 480

#define SW1_PIN	160
#define SW2_PIN	161
#define SW3_PIN	163
#define SW4_PIN	164
#define SENSOR	165
#define PI 3.1415926

char analyzeFrame(const VideoFrameRef& frame_depth,const VideoFrameRef& frame_color,Mat& depth_img, Mat& color_img) {
    DepthPixel* depth_img_data;
    RGB888Pixel* color_img_data;

    int w = frame_color.getWidth();
    int h = frame_color.getHeight();

    depth_img = Mat(h, w, CV_16U);
    color_img = Mat(h, w, CV_8UC3);
    Mat depth_img_8u;
	

    depth_img_data = (DepthPixel*)frame_depth.getData();

    memcpy(depth_img.data, depth_img_data, h*w*sizeof(DepthPixel));

    normalize(depth_img, depth_img_8u, 255, 0, NORM_MINMAX);

    depth_img_8u.convertTo(depth_img_8u, CV_8U);
    color_img_data = (RGB888Pixel*)frame_color.getData();

    memcpy(color_img.data, color_img_data, h*w*sizeof(RGB888Pixel));

    cvtColor(color_img, color_img, COLOR_RGB2BGR);

    return 'c';
}



int alpha_ = 10, beta_ = 90, gamma_ = 90;
int f_ = 1500, dist_ = 126, y = 136, scale_x = 10;

void process(int track, void *userdata){
     Mat img = *((Mat*)userdata);

    conf::H_LAYER = (int)(conf::H_ROI / conf::NUMLAYERS);
        conf::W_LAYER = conf::W_ROI;
    getMatrixWrap(imp::matrixWrap, conf::W_CUT, conf::H_CUT, imp::ALPHA,
                      imp::F, imp::DIST, imp::Y, imp::SCALE_X);
    imp::inverMatrixWrap = imp::matrixWrap.clone().inv();

    std::vector<double> angles;
    cv::Mat hsv, gray;
    cv::cvtColor(img, hsv, cv::COLOR_BGR2HSV);
    cv::cvtColor(img, gray, cv::COLOR_BGR2GRAY);
    processImg(hsv, gray, angles);
}

int main( int argc, char* argv[] ) {

    Status rc;
    Device device;

    VideoStream depth, color;
    rc = OpenNI::initialize();
    if (rc != STATUS_OK) {
        printf("Initialize failed\n%s\n", OpenNI::getExtendedError());
        return 0;
    }
    rc = device.open(ANY_DEVICE);
    if (rc != STATUS_OK) {
        printf("Couldn't open device\n%s\n", OpenNI::getExtendedError());
        return 0;
    }
    if (device.getSensorInfo(SENSOR_DEPTH) != NULL) {
        rc = depth.create(device, SENSOR_DEPTH);
        if (rc == STATUS_OK) {
            VideoMode depth_mode = depth.getVideoMode();
            depth_mode.setFps(30);
            depth_mode.setResolution(VIDEO_FRAME_WIDTH, VIDEO_FRAME_HEIGHT);
            depth_mode.setPixelFormat(PIXEL_FORMAT_DEPTH_100_UM);
            depth.setVideoMode(depth_mode);

            rc = depth.start();
            if (rc != STATUS_OK) {
                printf("Couldn't start the color stream\n%s\n", OpenNI::getExtendedError());
            }
        }
        else {
            printf("Couldn't create depth stream\n%s\n", OpenNI::getExtendedError());
        }
    }

    if (device.getSensorInfo(SENSOR_COLOR) != NULL) {
        rc = color.create(device, SENSOR_COLOR);
        if (rc == STATUS_OK) {
            VideoMode color_mode = color.getVideoMode();
            color_mode.setResolution(VIDEO_FRAME_WIDTH, VIDEO_FRAME_HEIGHT);
            color_mode.setPixelFormat(PIXEL_FORMAT_RGB888);
            color.setVideoMode(color_mode);

            rc = color.start();
            if (rc != STATUS_OK)
            {
                printf("Couldn't start the color stream\n%s\n", OpenNI::getExtendedError());
            }
        }
        else {
            printf("Couldn't create color stream\n%s\n", OpenNI::getExtendedError());
        }
    }
    
    VideoFrameRef frame_depth, frame_color;
    VideoStream* streams[] = {&depth, &color};
    
    bool is_save_file = false; // set is_save_file = true if you want to log video and i2c pwm coeffs.
    VideoWriter depth_videoWriter;	
    VideoWriter color_videoWriter;
    VideoWriter gray_videoWriter;
     
    string gray_filename = "gray.avi";
	string color_filename = "color.avi";
	string depth_filename = "depth.avi";
	
	Mat depthImg, colorImg, grayImage;
	int codec = CV_FOURCC('D','I','V', 'X');
	int video_frame_width = VIDEO_FRAME_WIDTH;
    int video_frame_height = VIDEO_FRAME_HEIGHT;
	Size output_size(video_frame_width, video_frame_height);

   	FILE *thetaLogFile; // File creates log of signal send to pwm control
	if(is_save_file) {
	    gray_videoWriter.open(gray_filename, codec, 8, output_size, false);
        color_videoWriter.open(color_filename, codec, 8, output_size, true);
        //depth_videoWriter.open(depth_filename, codec, 8, output_size, false);
        thetaLogFile = fopen("thetaLog.txt", "w");
	}
/// End of init logs phase ///

    int dir = 0, throttle_val = 0;
    double theta = 0;
    int current_state = 0;
    char key = 0;


    
    
    // Argc == 2 eg ./test-autocar 27 means initial throttle is 27
    int frame_width = VIDEO_FRAME_WIDTH;
    int frame_height = VIDEO_FRAME_HEIGHT;

    bool running = false, started = false, stopped = false;

    double st = 0, et = 0, fps = 0;
    double freq = getTickFrequency();


    bool is_show_cam = true;
	int count_s,count_ss;
    int frame_id = 0;
	vector<cv::Vec4i> lines;

    init("/home/ubuntu/Desktop/prototype");  

	namedWindow("Result", 1);

   createTrackbar("Alpha", "Result", &imp::ALPHA, 30, process, (void*)(&colorImg));
    createTrackbar("Distance", "Result", &imp::DIST, 500, process, (void*)(&colorImg));
    createTrackbar("Y", "Result", &imp::Y, 200, process, (void*)(&colorImg));
    createTrackbar("Thres", "Result", &conf::THRES, 255, process, (void*)(&colorImg));
    createTrackbar("Scle X", "Result", &imp::SCALE_X, 20, process, (void*)(&colorImg));
     createTrackbar("Down Y", "Result", &conf::DOWN_Y, 300, process, (void*)(&colorImg));
     createTrackbar("dis y inject", "Result", &conf::DOWN_Y_INJEC, 300, process, (void*)(&colorImg));
     createTrackbar("DIS_ROAD_INJUNCTION", "Result", &conf::DIS_ROAD_INJUNCTION, 300, process, (void*)(&colorImg));

     createTrackbar("Min Point", "Result", &conf::MIN_POINTS, 15, process, (void*)(&colorImg));
     createTrackbar("min width cnt", "Result", &conf::MIN_WIDTH_CNT, 60, process, (void*)(&colorImg));
    createTrackbar("Min Area", "Result", &conf::MIN_AREA_CNT, 200, process, (void*)(&colorImg));
    createTrackbar("Num layer", "Result", &conf::NUMLAYERS, 40, process, (void*)(&colorImg));
    createTrackbar("Dist two p", "Result", &conf::MIN_DIS_TWO_POINTS, 100, process, (void*)(&colorImg));
    createTrackbar("gap top", "Result", &conf::DIS_GEN_LINE_TOP, 150, process, (void*)(&colorImg));
    createTrackbar("gap bot", "Result", &conf::DIS_GEN_LINE_BOT, 150, process, (void*)(&colorImg));



    while ( true )
    { 

        st = getTickCount();
       
		depth.readFrame(&frame_depth);
		color.readFrame(&frame_color);
            	frame_id ++;
		char recordStatus = analyzeFrame(frame_depth,frame_color, depthImg, colorImg);
		flip(depthImg, depthImg, 1);
		flip(colorImg, colorImg, 1);
		
            ////////// Detect Center Point ////////////////////////////////////
            if (recordStatus == 'c') {
                cvtColor(colorImg, grayImage, CV_BGR2GRAY);
                cv::imshow("two", colorImg);

                process(0, (void*)(&colorImg));
            }

            et = getTickCount();
            fps = 1.0 / ((et-st)/freq);
            cerr << "FPS: "<< fps<< '\n';

            int key = waitKey(10) & 0xff;
            if(key == 32){
                waitKey();
            }

            if( key == 27 ) break;
        
      
    }

	if(is_save_file)
    {
        gray_videoWriter.release();
        color_videoWriter.release();
        //depth_videoWriter.release();
        fclose(thetaLogFile);
	}
    return 0;
}


