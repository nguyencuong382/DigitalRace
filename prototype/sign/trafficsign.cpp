//
// Created by nc on 11/04/2018.
//

#include "trafficsign.h"
#include <opencv2/imgproc/imgproc.hpp>
#include "traconf.h"
#include <iostream>
#include <traffic.h>

void TrafficSign::init(std::string path_model){

    try {
        std::cout << '\n' <<"model traffic sign ----------------" << std::endl;
        std::cout << "Reading: " <<  path_model << std::endl;
        this->model = cv::ml::SVM::create();
        this->model = cv::ml::SVM::load(path_model);
        std::cout << "Model loaded successfully." << std::endl << std::endl;
    } catch (const std::exception& e) {
        std::cout << "Loading model failed." << std::endl;
    }

    cv::Mat img_compute = cv::Mat(cv::Size(64,48), CV_8UC1);
    this->hog = cv::HOGDescriptor(cv::Size(64,48), cv::Size(8,8), cv::Size(4,4), cv::Size(4,4), 9);
    this->hog.compute(img_compute, this->size_);
    this->col = this->size_.size();
    this->input_predict = cv::Mat(1, col, CV_32FC1);
    this->size_predict_image = cv::Size(64,48);

}

int TrafficSign::find(cv::Mat &hsv, cv::Mat &gray){
    std::vector<cv::Rect> boxes;
    cv::Mat roi_hsv, roi_gray, sub_img;

    roi_hsv = hsv(tra::roi);
    roi_gray = gray(tra::roi);

    getBoxes(roi_hsv, boxes);
    groupBoxes(boxes);

    int id;
    for(cv::Rect &r : boxes){
        roi_gray(r).copyTo(sub_img);

        id = this->predict(sub_img);
        if(id == 1 || id == 0){

            if(tra::DEBUG){
                cv::rectangle(roi_hsv, r, cv::Scalar(0, 255, 255), 2);
            }
        }


    }

    if(tra::DEBUG){
        for(cv::Rect &r : boxes){
            cv::rectangle(roi_hsv, r,cv::Scalar(255,255,255));
        }

        cv::imshow("traffic", roi_hsv);
    }

    return id;
}

int TrafficSign::predict(cv::Mat &src) {
    cv::resize(src,src, this->size_predict_image);

    std::vector<float> des;
    this->hog.compute(src,des);

    for (int i = 0; i < this->col; i++){
        this->input_predict.at<float>(0,i)= des[i];
    }

    return (int)this->model->predict(input_predict);
}

void groupBoxes(std::vector<cv::Rect> &boxes){
    int area;
    double area_ratio;

    if(boxes.size() >= 2) {
        for (unsigned int i = 0; i < boxes.size() - 1; i++) {

            for (unsigned int j = i + 1; j < boxes.size(); j++) {

                area = (boxes[i] & boxes[j]).area();

                if (area > 0) {

                    area_ratio = (double) area / (double) std::min(boxes[i].area(), boxes[j].area());

                    if (area_ratio >= tra::INTERS_AREA_RATIO) {

                        boxes[i] = boxes[i] | boxes[j];

                        boxes.erase(boxes.begin() + j);
                        j--;
                    } // end check ratio
                } // end check area
            } // endl loop box j
        } // end loop box i
    } // end check size of boxes
}

void getBoxes(cv::Mat &hsv, std::vector<cv::Rect> &boxes){
    cv::Mat mask;
    cv::inRange(hsv, tra::lowHSV, tra::highHSV, mask);
    std::vector<std::vector<cv::Point>> contours;
    std::vector<cv::Vec4i> hierarchy;

    if(tra::DEBUG){
        cv::imshow("mask1", mask);
    }

    findContours(mask, contours, hierarchy, cv::RETR_TREE , cv::CHAIN_APPROX_SIMPLE);

    for(unsigned int i = 0; i < contours.size(); i++){
        cv::Rect r = cv::boundingRect(contours[i]);

        double ratio = (double)r.width/(double)r.height;

        int area = r.area();

        if(area >= tra::MIN_CNT && area <= tra::MAX_CNT
                && ratio >= tra::L_RATIO && ratio <= tra::H_RATIO){
            boxes.push_back(r);
        }
    }
}

namespace sign{
    TrafficSign findTraffic = TrafficSign();
}