//
// Created by nc on 11/04/2018.
//

#ifndef CDS_TRAFFICSIGN_H
#define CDS_TRAFFICSIGN_H

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/ml/ml.hpp>
#include <opencv2/objdetect/objdetect.hpp>

#include <dirent.h>
#include <string>

class TrafficSign{
public:
    cv::Ptr<cv::ml::SVM> model;
    cv::HOGDescriptor hog;
    std::vector<float> size_;
    int col;
    cv::Mat input_predict;
    cv::Size size_predict_image;

    int find(cv::Mat &, cv::Mat &);
    int predict(cv::Mat &);
    void init(std::string);
};

namespace sign{
    extern TrafficSign findTraffic;
}

void getBoxes(cv::Mat &src, std::vector<cv::Rect> &boxes);
void groupBoxes(std::vector<cv::Rect> &boxes);

#endif //CDS_TRAFFICSIGN_H
