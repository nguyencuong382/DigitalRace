#include "getparam.h"
#include "frameprocess.h"
#include "green.h"
#include "secret.h"
#include "detectorobj.h"

int test(std::string filename, int MODE){
    cv::VideoCapture video(filename);

    if(!video.isOpened()){
        std::cout << "OPEN VIDEO FAILED!" << std::endl;
        return -1;
    }

    double total_time = 0;
    int num_frames = 0;

    double freq = cv::getTickFrequency();
    double st = 0, et = 0, fps = 0;
    double sum_fps = 0;

    while(true){
        st = cv::getTickCount();

        cv::Mat frame;
        video >> frame;

        if(frame.empty()){
            std::cout << "Failed read frame" << std::endl;
            break;
        }
        num_frames ++;
        if(num_frames < 520){
            // continue;
        }

        if(MODE == 1){
            cv::resize(frame, frame, cv::Size(conf::WIDTH, conf::HEIGHT));
            cv::Mat depth;
            cv::Rect obj(120,120,40,40);
//            status::OBJ = objdetect::detector.get_object_depth(depth, frame, obj);
            status::OBJ = false;
            updateObj(obj, 1,1);
            processImg(frame);
        } else if (MODE == 2){

        } else if (MODE == 3){
            cv::Mat hsv;
            cv::cvtColor(frame, hsv, cv::COLOR_RGB2BGR);

            cv::cvtColor(frame, hsv, cv::COLOR_BGR2HSV);
            std::vector<std::vector<cv::Point>> contours;
            findContoursNormColor(hsv, contours);
            cv::Mat mask = cv::Mat::zeros(hsv.size(), CV_8UC3);
            cv::drawContours(mask, contours, -1 , cv::Scalar(255), 1 , 8 );
            cv::imshow("mask green", mask);
            cv::imshow("src", frame);
        } else if (MODE == 4){
            cv::resize(frame, frame, cv::Size(conf::WIDTH, conf::HEIGHT));
            cv::Mat depth;
            cv::Rect obj(120,120,40,40);
            status::OBJ = objdetect::detector.get_object_depth(depth, frame, obj);
            updateObj(obj, 1,1);
            processImgSe(frame);
        }

        int k = cv::waitKey(conf::WAIT_KEY) & 0xff;

        if(k == 27){
            break;
        }
        if(k == 32){
            cv::waitKey();
        }

        et = cv:: getTickCount();
        sum_fps += 1.0 / ((et-st)/freq);
        std::cerr << "FPS: "<< sum_fps/num_frames << '\n';

    }
    video.release();
    cv::destroyAllWindows();
    return 1;
}

int main(){
    init("../");
    test("../main/25.avi", 1 );
}
