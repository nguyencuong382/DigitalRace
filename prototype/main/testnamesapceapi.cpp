//
// Created by nc on 20/04/2018.
//

#include "iostream"
#include "testnamesapceapi.h"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "math.h"
#include <cmath>
#include <vector>

using namespace std;
using namespace cv;

void drawPara(cv::Size t,
              std::vector<double> coef,
              std::vector<double> xV,
              std::vector<double> yV ){

    cv::Mat mask = cv::Mat::zeros(t, CV_8UC1);

    std::vector<cv::Point2f> list_point(t.width);
    for(int x = 0; x < t.width; x++){
        double xa = (double)x;
        double y = coef[0] + coef[1] * xa + coef[2] * xa * xa;
        list_point[x].x = x;
        list_point[x].y = y;
    }

    for(int i = 0; i < xV.size(); i++){
        cv::circle(mask, cv::Point(xV[i], yV[i]), 3, cv::Scalar(255,255,255), 2);
    }

    for(int i = 1; i < t.width; i++){
        cv::line(mask,list_point[i-1],list_point[i],Scalar(255,255,255), 1);
    }

    cv::imshow("para", mask);
    cv::waitKey();

}

#include<iostream>
#include<vector>
#include<algorithm>
#include<cmath>
using namespace std;

typedef vector<double> vec;

struct SplineSet{
    double a;
    double b;
    double c;
    double d;
    double x;
};

vector<SplineSet> spline(vec &x, vec &y)
{
    int n = x.size()-1;
    vec a;
    a.insert(a.begin(), y.begin(), y.end());
    vec b(n);
    vec d(n);
    vec h;

    for(int i = 0; i < n; ++i)
        h.push_back(x[i+1]-x[i]);

    vec alpha;
    for(int i = 0; i < n; ++i)
        alpha.push_back( 3*(a[i+1]-a[i])/h[i] - 3*(a[i]-a[i-1])/h[i-1]  );

    vec c(n+1);
    vec l(n+1);
    vec mu(n+1);
    vec z(n+1);
    l[0] = 1;
    mu[0] = 0;
    z[0] = 0;

    for(int i = 1; i < n; ++i)
    {
        l[i] = 2 *(x[i+1]-x[i-1])-h[i-1]*mu[i-1];
        mu[i] = h[i]/l[i];
        z[i] = (alpha[i]-h[i-1]*z[i-1])/l[i];
    }

    l[n] = 1;
    z[n] = 0;
    c[n] = 0;

    for(int j = n-1; j >= 0; --j)
    {
        c[j] = z [j] - mu[j] * c[j+1];
        b[j] = (a[j+1]-a[j])/h[j]-h[j]*(c[j+1]+2*c[j])/3;
        d[j] = (c[j+1]-c[j])/3/h[j];
    }

    vector<SplineSet> output_set(n);
    for(int i = 0; i < n; ++i)
    {
        output_set[i].a = a[i];
        output_set[i].b = b[i];
        output_set[i].c = c[i];
        output_set[i].d = d[i];
        output_set[i].x = x[i];
    }
    return output_set;
}

void estlines()
{
    vec x(11);
    vec y(11);
    for(int i = 0; i < x.size(); ++i)
    {
        x[i] = i;
        y[i] = sin(i);
    }

    vector<SplineSet> cs = spline(x, y);
    for(int i = 0; i < cs.size(); ++i){
        cout << "a " << cs[i].d << "\t" << cs[i].c << "\t" << cs[i].b << "\t" << cs[i].a << endl;
    }
}



void print(vector< vector<double> > A) {
    int n = A.size();
    for (int i=0; i<n; i++) {
        for (int j=0; j<n+1; j++) {
            cout << A[i][j] << "\t";
            if (j == n-1) {
                cout << "| ";
            }
        }
        cout << "\n";
    }
    cout << endl;
}

vector<double> gauss(vector< vector<double> > A) {
    int n = A.size();

    for (int i=0; i<n; i++) {
        // Search for maximum in this column
        double maxEl = abs(A[i][i]);
        int maxRow = i;
        for (int k=i+1; k<n; k++) {
            if (abs(A[k][i]) > maxEl) {
                maxEl = abs(A[k][i]);
                maxRow = k;
            }
        }

        // Swap maximum row with current row (column by column)
        for (int k=i; k<n+1;k++) {
            double tmp = A[maxRow][k];
            A[maxRow][k] = A[i][k];
            A[i][k] = tmp;
        }

        // Make all rows below this one 0 in current column
        for (int k=i+1; k<n; k++) {
            double c = -A[k][i]/A[i][i];
            for (int j=i; j<n+1; j++) {
                if (i==j) {
                    A[k][j] = 0;
                } else {
                    A[k][j] += c * A[i][j];
                }
            }
        }
    }

    // Solve equation Ax=b for an upper triangular matrix A
    vector<double> x(n);
    for (int i=n-1; i>=0; i--) {
        x[i] = A[i][n]/A[i][i];
        for (int k=i-1;k>=0; k--) {
            A[k][n] -= A[k][i] * x[i];
        }
    }
    return x;
}

int testlinear() {
    int n;
    cin >> n;

    vector<double> line(n+1,0);
    vector< vector<double> > A(n,line);

    // Read input data
    for (int i=0; i<n; i++) {
        for (int j=0; j<n; j++) {
            cin >> A[i][j];
        }
    }

    for (int i=0; i<n; i++) {
        cin >> A[i][n];
    }

    // Print input
    print(A);

    // Calculate solution
    vector<double> x(n);
    x = gauss(A);

    // Print result
    cout << "Result:\t";
    for (int i=0; i<n; i++) {
        cout << x[i] << " ";
    }
    cout << endl;
}



int main(){

    cv::Point p1 (2,3);
    cv::Point p2 (5,6);

    std::vector<int> a;
    a.push_back(3);
    a.push_back(2);

    a.insert(a.begin() + 1, 1);

    for(auto &i : a){
        std::cout << i << " ";
    } std::cout << std::endl;

    int index = 1;
    a.erase(a.begin(), a.begin() + index);
    for(auto &i : a){
        std::cout << i << " ";
    } std::cout << std::endl;

    a.erase(a.begin() + 1, a.end());
    for(auto &i : a){
        std::cout << i << " ";
    } std::cout << std::endl;

    //estlines();
//    testlinear();
    const unsigned int order = 2;
    unsigned int countOfElements = 3;
    int result;
    std::vector<double> dependentValues, independentValues, coef;
    dependentValues.push_back(9); independentValues.push_back(313);
    dependentValues.push_back(11); independentValues.push_back( 305);
    dependentValues.push_back(12); independentValues.push_back( 292);
    dependentValues.push_back(15); independentValues.push_back( 284);
    dependentValues.push_back(19); independentValues.push_back( 273);
    dependentValues.push_back(21); independentValues.push_back(265);
    dependentValues.push_back(25); independentValues.push_back( 255);
    dependentValues.push_back(29); independentValues.push_back( 244);
    dependentValues.push_back(33); independentValues.push_back( 233);
    dependentValues.push_back(36); independentValues.push_back( 222);
    dependentValues.push_back(40); independentValues.push_back( 212);
    dependentValues.push_back(43); independentValues.push_back( 203);
    dependentValues.push_back(47); independentValues.push_back( 193);
    dependentValues.push_back(49); independentValues.push_back( 185);
    dependentValues.push_back(53); independentValues.push_back( 175);
    dependentValues.push_back(57); independentValues.push_back( 165);
    dependentValues.push_back(60); independentValues.push_back( 156);
    dependentValues.push_back(63); independentValues.push_back( 147);
    dependentValues.push_back(67); independentValues.push_back( 135);
    dependentValues.push_back(71); independentValues.push_back( 124);
    dependentValues.push_back(74); independentValues.push_back( 115);
    dependentValues.push_back(77); independentValues.push_back( 104);
    dependentValues.push_back(71); independentValues.push_back( 92);


    coef = polyfit(dependentValues, independentValues, 2);
    for(double a: coef){
        std::cout<< a << " ";
    } std::cout << endl;



//    drawPara(cv::Size(320,320), coef, dependentValues, independentValues);
}

std::vector<double> polyfit(const std::vector<cv::Point>   points,
                            int                            nDegree){

    // more intuative this way
    nDegree++;

    unsigned int nCount =  points.size();
    boost::numeric::ublas::matrix<double> oXMatrix( nCount, nDegree );
    boost::numeric::ublas::matrix<double> oYMatrix( nCount, 1 );

    // copy y matrix
    for ( size_t i = 0; i < nCount; i++ )
    {
        oYMatrix(i, 0) = points[i].y;
    }

    // create the X matrix
    for ( size_t nRow = 0; nRow < nCount; nRow++ )
    {
        double nVal = 1.0f;
        for ( int nCol = 0; nCol < nDegree; nCol++ )
        {
            oXMatrix(nRow, nCol) = nVal;
            nVal *= points[nRow].x;
        }
    }

    // transpose X matrix
    boost::numeric::ublas::matrix<double> oXtMatrix( trans(oXMatrix) );
    // multiply transposed X matrix with X matrix
    boost::numeric::ublas::matrix<double> oXtXMatrix( prec_prod(oXtMatrix, oXMatrix) );
    // multiply transposed X matrix with Y matrix
    boost::numeric::ublas::matrix<double> oXtYMatrix( prec_prod(oXtMatrix, oYMatrix) );
    // lu decomposition
    boost::numeric::ublas::permutation_matrix<int> pert(oXtXMatrix.size1());

    const std::size_t singular = boost::numeric::ublas::lu_factorize(oXtXMatrix, pert);
    // must be singular
    if( singular == 0 ){
        // backsubstitution
        boost::numeric::ublas::lu_substitute(oXtXMatrix, pert, oXtYMatrix);
        // copy the result to coeff
        return std::vector<double>( oXtYMatrix.data().begin(), oXtYMatrix.data().end() );
    }
    return std::vector<double>(3);
}


int polyfit2(std::vector<double> dependentValues,
             std::vector<double> independentValues,
             unsigned int        countOfElements,
             unsigned int        order,
             double*             coefficients)
{
    // Declarations...
    // ----------------------------------
    enum {maxOrder = 5};

    double B[maxOrder+1] = {0.0f};
    double P[((maxOrder+1) * 2)+1] = {0.0f};
    double A[(maxOrder + 1)*2*(maxOrder + 1)] = {0.0f};

    double x, y, powx;

    unsigned int ii, jj, kk;

    // Verify initial conditions....
    // ----------------------------------

    // This method requires that the countOfElements >
    // (order+1)
    if (countOfElements <= order)
        return -1;

    // This method has imposed an arbitrary bound of
    // order <= maxOrder.  Increase maxOrder if necessary.
    if (order > maxOrder)
        return -1;

    // Begin Code...
    // ----------------------------------

    // Identify the column vector
    for (ii = 0; ii < countOfElements; ii++)
    {
        x    = dependentValues[ii];
        y    = independentValues[ii];
        powx = 1;

        for (jj = 0; jj < (order + 1); jj++)
        {
            B[jj] = B[jj] + (y * powx);
            powx  = powx * x;
        }
    }

    // Initialize the PowX array
    P[0] = countOfElements;

    // Compute the sum of the Powers of X
    for (ii = 0; ii < countOfElements; ii++)
    {
        x    = dependentValues[ii];
        powx = dependentValues[ii];

        for (jj = 1; jj < ((2 * (order + 1)) + 1); jj++)
        {
            P[jj] = P[jj] + powx;
            powx  = powx * x;
        }
    }

    // Initialize the reduction matrix
    //
    for (ii = 0; ii < (order + 1); ii++)
    {
        for (jj = 0; jj < (order + 1); jj++)
        {
            A[(ii * (2 * (order + 1))) + jj] = P[ii+jj];
        }

        A[(ii*(2 * (order + 1))) + (ii + (order + 1))] = 1;
    }

    // Move the Identity matrix portion of the redux matrix
    // to the left side (find the inverse of the left side
    // of the redux matrix
    for (ii = 0; ii < (order + 1); ii++)
    {
        x = A[(ii * (2 * (order + 1))) + ii];
        if (x != 0)
        {
            for (kk = 0; kk < (2 * (order + 1)); kk++)
            {
                A[(ii * (2 * (order + 1))) + kk] =
                        A[(ii * (2 * (order + 1))) + kk] / x;
            }

            for (jj = 0; jj < (order + 1); jj++)
            {
                if ((jj - ii) != 0)
                {
                    y = A[(jj * (2 * (order + 1))) + ii];
                    for (kk = 0; kk < (2 * (order + 1)); kk++)
                    {
                        A[(jj * (2 * (order + 1))) + kk] =
                                A[(jj * (2 * (order + 1))) + kk] -
                                y * A[(ii * (2 * (order + 1))) + kk];
                    }
                }
            }
        }
        else
        {
            // Cannot work with singular matrices
            return -1;
        }
    }

    // Calculate and Identify the coefficients
    for (ii = 0; ii < (order + 1); ii++)
    {
        for (jj = 0; jj < (order + 1); jj++)
        {
            x = 0;
            for (kk = 0; kk < (order + 1); kk++)
            {
                x = x + (A[(ii * (2 * (order + 1))) + (kk + (order + 1))] *
                         B[kk]);
            }
            coefficients[ii] = x;
        }
    }

    return 0;
}


