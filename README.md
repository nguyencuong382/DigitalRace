# DigitalRace Autonomous Car

Cross + Green lane
![image_demo_1](https://gitlab.com/nguyencuong382/DigitalRace/raw/master/image/demo_1.png)

White lane
![image_demo_1](https://gitlab.com/nguyencuong382/DigitalRace/raw/master/image/demo_2.png)
## Setup
All essential files are on opencv_setup branch
### Setup OpenCV
Run [the script](https://gitlab.com/nguyencuong382/DigitalRace/blob/opencv_setup/opencv.sh) by command:
```
sudo sh opencv.sh
```
* Note that the project run on **OpenCV 3.3.0**, I DO NOT recommand higher version because it doesn't support some functions and operators like Opencv 4.0

You can check if OpenCV was installed
```
pkg-config opencv --cflags
pkg-config opencv --libs
```
You may get an error "Package opencv was not found in the pkg-config search path". To fix it:
```
export PKG_CONFIG_PATH=/usr/local/lib/pkgconfig
```
To test version of OpenCV. Go to [version_opencv](https://gitlab.com/nguyencuong382/DigitalRace/tree/opencv_setup/version_opencv) folder and run script
```
sh versionOpencv.sh
```
### Setup Boot
You can use apt-get command (requires sudo)
```
sudo apt-get install libboost-all-dev
```
Or you can call
```
aptitude search boost
```
## Run project
```
bash run.sh
```
